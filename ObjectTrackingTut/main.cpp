#include <opencv\cv.h>
#include <opencv\highgui.h>

using namespace cv;

int main() {

	//Cria uma matriz para armazenar a filmagem;
	Mat filme;
	VideoCapture cap;
	cap.open(0);

	//Cria uma tela para mostrar a filmagem;
	namedWindow("window", 1);

	while (1) {
	
		//Copia o v�deo para a matriz;
		cap >> filme;

		//Apresenta o filme na tela;
		imshow("window", filme);

		//Cria um delay de 33s;
		waitKey(33);
	}

	return 0;
}